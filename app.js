const form = document.getElementById("film-form");
const titleElement = document.querySelector("#title");
const directorElement = document.querySelector("#director");
const urlElement = document.querySelector("#url");
const cardBody = document.querySelectorAll(".card-body")[1];
const clear = document.querySelector("#clear-films");

//TÜM EVENTLERİ BAŞLATMA
eventListeners();
function eventListeners(){
    clear.addEventListener("click",clearAllFilms);
    cardBody.addEventListener("click",deleteFilm);
    document.addEventListener("DOMContentLoaded",()=>{
        let films = Storage.getFilmsFromStorage();
        UI.loadAllFilms(films);
    })
    form.addEventListener("submit",addFilm);
};
function deleteFilm(e){
    if(e.target.id == "delete-film"){
        UI.deleteFilmFromUI(e.target);
        Storage.deleteFilmFromStorage(e.target.parentElement.previousElementSibling.previousElementSibling.textContent);
    }
}
function addFilm(e){
    e.preventDefault();
    const title = titleElement.value;
    const director = directorElement.value;
    const url = urlElement.value;
    if (title == "" || url == "" || director == "") {
        //Display Error Message from using ui class methods
        UI.displayMessage("Boş alan bırakmadan ekleyin!","danger");
    }else{
        const newFilm = new Film(title,director,url);
        Storage.addFilmToStorage(newFilm);
        UI.addFilmToUI(newFilm); //Arayüze Film ekleme
        UI.clearInputs(titleElement,urlElement,directorElement);
        UI.displayMessage("Film Başarı ile eklendi","success")
    }
   
}
function clearAllFilms(){
    UI.clearAllFilmsFromUI();
    Storage.clearAllFilmsFromStorage();
    UI.displayMessage("Tüm filmler Silindi..","warning");
}