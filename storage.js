class  Storage{

    static addFilmToStorage(newFilm){
        let films = this.getFilmsFromStorage();
        console.log(films);
        films.push(newFilm);
        localStorage.setItem('films',JSON.stringify(films));
        
        console.log("Ekleme Yapıldı"+JSON.stringify(films));
        }
       static  getFilmsFromStorage(){
            let films;
            if(localStorage.getItem("films") === null){
                films=[];
                console.log("Boş film dizisi döndürüldü")
            }else{
                films = JSON.parse(localStorage.getItem("films"));
                console.log("dolu film dizisi döndürüldü")
            }
            return films;
        }
        static deleteFilmFromStorage(filmTitle){
            let films = this.getFilmsFromStorage();
            films.forEach((film,index)=>{
                if(film.title === filmTitle){
                    films.splice(index,1);
                    
                }
            });
            localStorage.setItem("films",JSON.stringify(films));
            UI.displayMessage("Silme İşlemi Başarılı","warning")
        }
        static clearAllFilmsFromStorage(){
            let films = [];
            localStorage.setItem("films",JSON.stringify(films));
        }

}
